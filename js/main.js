"use strict";

// tableau d'objet
const photos = [
  { src: "1.jpg", legend: "Jean-Michel Basquiat (Brooklyn, 1960-1988)" },
  { src: "2.jpg", legend: "Jean-Michel Basquiat (Brooklyn, 1960-1988)" },
  { src: "3.jpg", legend: "Jean-Michel Basquiat (Brooklyn, 1960-1988)" },
  { src: "4.jpg", legend: "Jean-Michel Basquiat (Brooklyn, 1960-1988)" },
  { src: "5.jpg", legend: "Jean-Michel Basquiat (Brooklyn, 1960-1988)" },
  { src: "6.jpg", legend: "Jean-Michel Basquiat (Brooklyn, 1960-1988)" },
];

//let state;

let state = new Object();
//console.table(photos[2].legend, // ce qu'on va utiliser
//console.table(photos[2].legend, photos[2]["legend"]);

//console.table(photos[2]); // pr recup l'index
//console.table(photos[2]).legend; // pr recup la valeur: legend
//console.table(photos[2]).src; // pr recup la valeur: src
//console.table(photos[2]).legend, photos[2]["legend"]; // pr recup une valeur dynamique

// first:
// sur le lien on supprimme la classe hide pr afficher l'élement
// pr que la flèche change de direction, on la met a down ds la classes
/*** FUNCTIONS ***/
/**
 * fonction qui va afficher la toolbar & l'icône du lien à mettre à down
 */
function toolbarWithToggle() {
  let icon;

  // je modifie l'icône du lien pour afficher ou cacher la barre d'outils.
  icon = document.querySelector("#toolbar-toggle i");

  icon.classList.toggle("fa-arrow-down");
  icon.classList.toggle("fa-arrow-right");

  //console.log("test OK");
  document.querySelector(".toolbar ul").classList.toggle("hide");
  document.querySelector(".fa fa-arrow-right");
  // mais on aurai pu le faire avec 1 replace et une condition comme if else (ms + compliqué)
}

/**
 * mettre à jour la photo et sa légende ds le carousel
 */
function refreshSlider() {
  // console.log("charg img");
  document.querySelector(".slider img").src =
    "images/" + photos[state.index].src;
  document.querySelector("#slider figcaption").textContent =
    photos[state.index].legend;
  //let sliderImage;
  //let sliderLegend;

  // Recherche des balises de contenu du carrousel.
  //sliderImage = document.querySelector(".slider img");
  //sliderLegend = document.querySelector("#slider figcaption");
  console.log("img");
  // Changement de la source de l'image et du texte de la légende du carrousel.
  //sliderImage.src = photos[state.index].image;
  //sliderLegend.textContent = photos[2][state.index].legend;
}

function nextBtnClick() {
  // console.log("next img"); // OK, gest event installé
  // changer le state.index
  // console.log("test", state.index);
  state.index++;
  // mettre une condition pr qd on arrive à la fin du tableau, il retourne au début
  // Est-ce qu'on est arrivé à la fin de la liste des slides ?
  if (state.index == photos.length) {
    // Oui, on revient au début (le carrousel est circulaire).
    state.index = 0;
  }
  // Mise à jour de l'affichage.
  refreshSlider();
}

// btn précédant
function previousBtnClick() {
  // console.log("next img"); // OK, gest event installé
  // changer le state.index
  // console.log("test", state.index);
  state.index--;
  if (state.index < 0) {
    state.index = photos.length - 1;
  }

  refreshSlider();
}

// 1. Faire ensuite la fonction pour l'affichage de photo aléatoire. On aura ici besoin
// d'une fonction qui renvoie un nombre aléatoire compris entre un minimum (0) et un
// maximum (la longueur du tableau de photos).
// Attention : on ne veut pas que la photo aléatoire soit déjà celle qui est affichée !
// Tant qu'il nous propose la même, on regénère un chiffre aléatoire !

// btn randoom
function randoomBtnClick() {
  let index;
  console.log("randoom img"); // OK, gest event installé
  // if ((state.index = photos.length)) {
  //   state.index = getRandomInteger;
  // }
  do {
    index = getRandomInteger(0, photos.length - 1);
  } while (index == state.index);
  state.index = index;

  refreshSlider();
}

/**
 * Lance le diaporama ttes les 2 secondes
 * @param
 * @param
 * @return
 */
function toggleBtnClick() {
  if (state.timer == false) {
    state.timer = window.setInterval(nextBtnClick, 1000);
  } else {
    window.clearInterval(state.timer);
    state.timer = false;
  }
  // je vise l'id et
  let icone = document.querySelector("#slider-toggle i");
  // je lui ajoute l'icone avec un toogle
  icone.classList.toggle("fa-play");
  icone.classList.toggle("fa-pause");
}

/*** CODE ***/
document.addEventListener("DOMContentLoaded", function () {
  //Initialiser le carrousel
  state = {};
  state.index = 0; // On commence à la première slide
  state.timer = false; // Le carrousel est arrêté au démarrage

  refreshSlider();
  // j cible la balise & j add 1 ecouteur qui appelle 1 fonction que j vais créer
  document
    .querySelector("#toolbar-toggle")
    .addEventListener("click", toolbarWithToggle);

  // gest event sur nextBtnClick
  document
    .querySelector("#slider-next")
    .addEventListener("click", nextBtnClick);
  //button id="slider-next" title="Image suivante"><i class="fa fa-forward"

  // gest event sur previousBtnClick
  document
    .querySelector("#slider-previous")
    .addEventListener("click", previousBtnClick);

  // gest event sur randoomBtnClick
  document
    .querySelector("#slider-random")
    .addEventListener("click", randoomBtnClick);

  // gest event sur btn play pr lancer 1 toogle sur la lecture du caroussel d'images
  document
    .querySelector("#slider-toggle")
    .addEventListener("click", toggleBtnClick);
});

// le DOMContentLoaded, dès qu'on doit avoir une relation avec le DOM (en général, le code principal)
// radar qui permet de d'executer le code qu'un fois que la page sera chargée
// et fonction anonyne (le code sera le code principal)

// second:
// put class= to class="fa fa-arrow-down"

// third: on va vouloir charger l'img

/*****************************************************************
 *  test perso sur la manupulation du DOM  ***********************
 * ***************************************************************/
/* effet de clignotement sur 1 seul element */
// j cible mon p avec sa classe .paragraph
// const p = document.querySelector(".paragraph");
// window.setInterval(function () {
//   // je plasse 1 toogle qui va enlever & remettre la classe bleu à chaque seconde (le 1er paragraph toggle sur blanc & bleu )
//   p.classList.toggle("blue");
// }, 1000);

/* ERREUR DE VARIABLE LOCALE: effet de clignotement sur ts les paragraphs (seul le second p est affecté car p a changé en 
  cours de route !!! */
// const ps = document.querySelectorAll(".paragraph");
// // comme c'est 1 tableau, faut boucleau sur ces élements
// for (let i = 0; i < ps.length; i++) {
//   // création d'1 variable p qui est le paragraph en cours
//   let p = ps[i];
//   // comme décris ci-dessous, je dois créer 1 fonction qui s'appelle elle même, que j'appelle en lui passant le paragraph,

//   // !!! ERREUR !!! ICI IL FAUT CREER 1 FONCTION  COMME J FAIS DS LE PROCHAIN CODE!!!

//   // je refais mon setInterval
//   window.setInterval(function () {
//     // ms attention ! p ci-dessous à changé et ne va donc etre affecté qu'au 2eme paragraph !!! -
//     // du coup, il faut créer une fonction ci-dessus, en dessous de la création de la variable p (le paragraph en cours)
//     p.classList.toggle("blue");
//   }, 1000);
// }

/* CORRIGE: */
/* ICI IL FAUT CREER 1 FONCTION qui s'auto appelle, */
/* VOIR L ERREUR DS LE DODE DU DESSUS !!! */
/* effet de clignotement sur ts les paragraphs */
// const ps = document.querySelectorAll(".paragraph");
// // comme c'est 1 tableau, faut boucleau sur ces élements
// for (let i = 0; i < ps.length; i++) {
//   // comme décris ci-dessous, je dois créer 1 fonction qui s'appelle elle même, que j'appelle en lui passant le paragraph,
//   (function (p) {
//     // je refais mon setInterval
//     window.setInterval(function () {
//       p.classList.toggle("blue");
//     }, 1000);
//   })(ps[i]);
// }
