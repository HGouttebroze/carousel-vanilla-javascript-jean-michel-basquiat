Afin de bien aborder l'exercice, voici les étapes que nous vous recommandons de respecter :

1. Prendre connaissance du fichier CSS
1. Dans le javascript, créer un tableau `slides` contenant les informations de chacune des images sous la forme d'un objet ayant pour propriété :
1. Créer une variable globale `state` qui sera un objet qui contiendra les informations sur l'état du carrousel (à quelle image on est, si le diapo est lancé)
1. Initialiser le carrousel. Attention, on va s'assurer que le code s'éxécute bien uniquement une fois que la page HTML est chargée ‣. Dans cette fonction anonyme, on va initialiser l'état du carrousel en indiquant que la première photo à afficher est celle à l'index 0 `state.index = 0;`
1. On va créer une fonction `refreshSlider()` qui se charge d'afficher une image (ici la 1ère) et qui sera appelée dès l'initialisation.
   Cette fonction se charge de cibler l'image et le figcaption, et de modifier respectivement le src et le texte.
   _Un figcaption est une balise qui permet d'ajouter une légende à une image ou un schéma contenu dans un `figure`_
1. Installer un gestionnaire d'événement sur `#toolbar-toggle` pour qu'au clic, cela affiche la UL contenant les différents boutons de contrôle.
1. Ensuite, installer un gestionnaire d'événement sur le bouton qui permet de passer à la photo suivante et coder la fonction associée.
   Cette fonction permet de passer à l'index de photo suivante puis de rafraichir l'affichage.
   Attention : une fois arrivé à la fin des photo, il faut revenir au début 😉
1. Faire la même chose pour le bouton qui permet de revenir à la photo précédente
   Attention, une fois arriver à la première photo, il faut revenir à la dernière !
1. Faire ensuite la fonction pour l'affichage de photo aléatoire. On aura ici besoin d'une fonction qui renvoie un nombre aléatoire compris entre un minimum (0) et un maximum (la longueur du tableau de photos).
   Attention : on ne veut pas que la photo aléatoire soit déjà celle qui est affichée ! Tant qu'il nous propose la même, on regénère un chiffre aléatoire !
1. Enfin, s'occuper du bouton play/pause qui permet de lancer ou arrêter le diaporama
